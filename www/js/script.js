var instCounter=0;
$(function(){
      
    $('select').change(function(){
        $('.alert').remove();
    });
    $('#ip').on('change keyup',function(){
        $('.alert').remove();
    });
 
    $('#ip').keydown(function(e){
        //msg(e.which);return false;
        if(e.which==13){
            $('#btn-calculate').trigger('click');
        }else if(e.which==116 || e.which==8 || e.which==190 || (e.which>47 && e.which<58)){
            return true;
        }
        $('.alert').remove();
        return false;
    });
    
    $('#btn-calculate').click(function(e){
        e.preventDefault();
        
        if(!ValidateIPaddress($('#ip').val().trim())){
            msg('Invalid IP Address, Please correct it.');
            $('.tab-wrapper').hide();
            $('.info').show();
            //$('#ip').focus();
        }else if(!$('#btn-calculate').hasClass('disabled')){
            
            var ip = $('#ip').val();
            var cidr = $('#cidr').val();
            var ipParts = $('#ip').val().split('.');
            var ipNum = ipParts[3];

            var power = 32-cidr;
            var ipLength = Math.pow(2,power);

            var networkIp = Math.floor(ipNum/ipLength)*ipLength;
            var broadcastIp = networkIp+ipLength-1;

            var ipPrefix = ipParts[0]+'.'+ipParts[1]+'.'+ipParts[2]+'.';
            
            var networks = 256/ipLength;

            var ipClass = 'Unknown';
            var ipClassNum = ipParts[0];
            switch(true) {
                case (ipClassNum<127):
                    ipClass = 'A';
                    break;
                case (ipClassNum<128):
                    ipClass = 'Loopback Range';
                    break;
                case (ipClassNum<192):
                    ipClass = 'B';
                    break;
                case (ipClassNum<224):
                    ipClass = 'C';
                    break;
                case (ipClassNum<256):
                    ipClass = 'D';
                    break;
                default:
                    ipClass = 'Unknown';
            }

            $('.tb-ip-lbl').html(ipLength);
            $('.tb-nw-lbl').html(networks);
            
            $('table.tbl-subnetting').html('<tr><td style="width:50%;">IP Class</td><th>'+ipClass+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>CIDR</td><th>'+cidr+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>Subnet mask</td><th>255.255.255.'+(256-ipLength)+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>Wildcard mask</td><th>0.0.0.'+(ipLength-1)+'</th></tr>');            
            $('table.tbl-subnetting').append('<tr><td># of networks</td><th>'+(networks)+'</th></tr>');

            $('table.tbl-subnetting').append('<tr><td colspan="2" class="divider1">&nbsp;</td></tr>');
            
            $('table.tbl-subnetting').append('<tr><td>Total IPs</td><th>'+ipLength+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>Usable IPs</td><th>'+(ipLength-2)+'</th></tr>');
            
            $('table.tbl-subnetting').append('<tr><td colspan="2" class="divider1">&nbsp;</td></tr>');

            $('table.tbl-subnetting').append('<tr><td>Network IP</td><th>'+ipPrefix+networkIp+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>First Host IP</td><th>'+ipPrefix+(networkIp+1)+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>Last Host IP</td><th>'+ipPrefix+(broadcastIp-1)+'</th></tr>');
            $('table.tbl-subnetting').append('<tr><td>Broadcast IP</td><th>'+ipPrefix+broadcastIp+'</th></tr>');
            
            
            
            
            /*ips list*/
            $('table.tbl-ips').html('<tr><td style="width:50%;"><strong>Network IP</strong></td><th>'+ipPrefix+networkIp+'</th></tr>');
            indx=1;
            for(i=(networkIp+1);i<=(broadcastIp-1);i++){
                $('table.tbl-ips').append('<tr><td>Host IP-'+indx+'</td><th>'+ipPrefix+i+'</th></tr>');
                indx++
            }
            $('table.tbl-ips').append('<tr><td><strong>Broadcast IP</strong></td><th>'+ipPrefix+broadcastIp+'</th></tr>');

            /*networks list*/
            $('table.tbl-networks').html('<tr><th>N/W No</th><th>From</th><th>To</th></tr>');
            
            var indx=1;
            for(n=0;n<256;n=n+ipLength){
                $('table.tbl-networks').append('<tr><td>Network-'+indx+'</td><td>'+ipPrefix+n+'</td><td>'+ipPrefix+(n+ipLength-1)+'</td></tr>');
                indx++;
            }
            $('.info').hide();
            $('.tab-wrapper').addClass('slideOut').show();
            $('.nav-tabs a.nav-link').eq(0).trigger('click');
            $('#resultTab,#ipsTab,#networksTab').scrollTop(0);
            $('#btn-calculate').addClass( "disabled");
            
        }
    });

    $(".tab-wrapper").bind('oanimationend animationend webkitAnimationEnd webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() { 
        if($(this).hasClass('slideOut')){
            $(this).removeClass('slideOut').addClass('slideIn');
        }else{
            $(this).removeClass('slideIn');
            $('#btn-calculate').removeClass( "disabled");
        }
     });
     
     $('.btn-discovery').click(function(e){
         e.preventDefault();
         $('.discovery-wrapper,.discovery-btn-wrapper').toggleClass('shown hidden');
         if(!$('body').hasClass('discovery')){
            $('body').toggleClass('discovery');
            //$('.discovery-btn-wrapper').removeClass('hidden').addClass('shown');
         }
     });

     $(".discovery-wrapper").bind('oanimationend animationend webkitAnimationEnd webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() { 
        if($(this).hasClass('hidden')){
            $('body').removeClass('discovery');
            //$('.discovery-btn-wrapper').removeClass('shown').addClass('hidden');
        }
     });

     /*share app link*/
	$('body').on('click','.btn-share-app',function(e){
		e.preventDefault();
		var options = {
          message: `SUBNET CALC\nI liked this App very much,\nI want you to install and see it has nice layout, animations and smooth transitions,\nOver all it has decent list of important information.`,
          subject: 'Install Subnet Calc App',
          //image url from google play
		  files:['https://lh3.googleusercontent.com/K3focWcTPJHwKFxJygdSXawxkAoVZ62BtJ9tb1VC4aRyjUBRUn-BMmVLv5iP-S_wQ1M=s180-rw'],
		  url: 'https://play.google.com/store/apps/details?id=com.codingsips.subnetting_calc',
		  chooserTitle: 'Send Subnet Calc app to a friend'
		};
		window.plugins.socialsharing.shareWithOptions(options, function(){}, function(){});
	});
});



function msg(str){
    $('.alert').remove();
    $('<div class="alert alert-warning alert-dismissible fade show" role="alert">'+str+
        `<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>`+'</div>')
      .appendTo('body')
      .delay(5000).queue(function() { $(this).remove(); });;
}
function ValidateIPaddress(ipaddress) {  
    if (/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/.test(ipaddress)) {  
        return true;  
    }   
    return false;  
}  

function ipvalidate(s) {
    var rgx = /^[0-9]*\.?[0-9]*$/;
    return s.match(rgx);
}

