var admobid = {};
isRewardReady = false;
if( /(android)/i.test(navigator.userAgent) ) { 
	admobid = { // for Android
		banner: 'ca-app-pub-7988186803056775/2579984001',
		interstitial: 'ca-app-pub-7988186803056775/2180528755',
		reward:'ca-app-pub-7988186803056775/6563169468'
	};
} else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
	admobid = { // for iOS
		banner: 'ca-app-pub-6869992474017983/4806197152',
		interstitial: 'ca-app-pub-6869992474017983/7563979554'
	};
} else {
	admobid = { // for Windows Phone
		banner: 'ca-app-pub-6869992474017983/8878394753',
		interstitial: 'ca-app-pub-6869992474017983/1355127956'
	};
}
 
function initApp() {
	try{
		if (AdMob) {
			AdMob.createBanner({
				adId : admobid.banner,
				position : AdMob.AD_POSITION.BOTTOM_CENTER,
				autoShow : true
			});
			$('.btn-reward').show();
		}else{
			$('.btn-reward').hide();
		}
	}catch(err){
		//msg(ex.message+' init');
	}
}

function instAd(){
	try{
		if(instCounter==0){
			if(AdMob) AdMob.prepareInterstitial( {adId:admobid.interstitial, autoShow:false} );
		}
		instCounter++;
		if(instCounter>5){
			instCounter=0;
			if(AdMob) AdMob.showInterstitial();		
		} 
	}catch(ex){
		//msg(ex.message+' inst');
	}
}
function prepareReward(){
	try{
		AdMob.prepareRewardVideoAd( {adId:admobid.reward, autoShow:false});
	}catch(ex){
		msg(ex.message+' reward prepare');
	}
}
function showReward(){
	try{
		document.addEventListener('onAdPresent', function(data){ 
			if(data.adType == 'rewardvideo') { 
				showRewardVideoAd();
			}
		});	
	}catch(ex){
		msg(ex.message+' reward show');
	}
}
$(function(){
	$('#btn-calculate').click(function(e){
		e.preventDefault();
		window.setTimeout( function(){
			instAd();
		}, 600 );
	});
	 
	
	$('.btn-reward').click(function(){
		prepareReward();
		showReward();
		prepareReward();
	});
});

document.addEventListener('deviceready', initApp, false);
